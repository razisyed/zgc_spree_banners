Spree::Core::Engine.routes.draw do

  # Add your extension routes here
    namespace :admin do
      resources :banners do
        collection do
          post :update_positions
        end
      end

      resources :banner_locations
    end
end
