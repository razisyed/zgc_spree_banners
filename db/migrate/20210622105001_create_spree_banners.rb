class CreateSpreeBanners < ActiveRecord::Migration[6.0]
  def change
    create_table :spree_banners do |t|
      t.string :name
      t.text :body
      t.string :link_url
      t.integer :position, null: false, default: 0
      t.integer :product_id
      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.datetime :image_updated_at
      t.boolean :published

      t.timestamps
    end
  end
end
