class CreateSpreeBannerLocation < ActiveRecord::Migration[6.0]
  def change
    create_table :spree_banner_locations do |t|
      t.string :name
      t.timestamps
    end
    create_table :spree_banner_banner_locations do |t|
      t.belongs_to :banner, index: true
      t.belongs_to :banner_location, index: true
    end
  end
end
