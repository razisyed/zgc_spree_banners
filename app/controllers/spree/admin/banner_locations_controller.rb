module Spree
  module Admin
    class BannerLocationsController < ResourceController
      respond_to :html

      def index
        @banner_locations = Spree::BannerLocation.order(:name)
      end
    end
  end
end