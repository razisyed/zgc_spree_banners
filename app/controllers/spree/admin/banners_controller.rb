module Spree
  module Admin
    class BannersController < ResourceController
      respond_to :html

      def index
        @banners = Spree::Banner.order(:position)
      end

      private

      def location_after_save
        if @banner.created_at == @banner.updated_at
          edit_admin_banner_url(@banner)
        else
          admin_banners_url
        end
      end

      def banner_params
        params.require(:banner).permit(:name, :body, :link_url, :published, :image, :position, :product_id)
      end
    end
  end
end