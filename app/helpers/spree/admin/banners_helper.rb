
module Spree
  module Admin
    module BannersHelper
      def get_column_header_by_type(type)
        return Spree.t(:name) if type == :image
        return Spree.t(:product) if type == :product

        return '----'
      end

      def get_image_link_by_type(banner, type)
        return link_to(banner.name, object_url(banner)) if type == :image
        return link_to(banner.product.name, object_url(banner)) if type == :product && banner.product

        return '----'
      end
    end
  end
end