module Spree
  module ProductDecorator

    def self.prepended(base)
      base.has_one :banner
      base.after_destroy :destroy_banner_if_deleted
    end

    def destroy_banner_if_deleted
      banner.update(published: false) if banner && deleted_at
    end

  end
end

::Spree::Product.prepend(Spree::ProductDecorator)
