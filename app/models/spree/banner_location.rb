class Spree::BannerLocation < ActiveRecord::Base
  has_and_belongs_to_many :banners,
                          class_name: 'Spree::Banner',
                          join_table: 'spree_banner_banner_locations'

  validates :name, presence: true
end